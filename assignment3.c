#include<stdio.h>
int main ()
{
	char ch;
	printf("Enter the character");
	scanf("%c",ch);
	if((ch>=48)&&(ch<=57))
	{
		printf("%c is a digit",ch);
	}
	else if((ch>=97)&&(ch<=122))
	{
		printf("%c is a lowercase alphabet",ch);
	}
	else if((ch>=65)&&(ch<=90))
	{
		printf("%c is a uppercase alphabet",ch);
	}
	else
	{
		printf("%c is a special character",ch);
	}
	return 0;
}