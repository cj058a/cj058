#include<stdio.h>
int main()
{
	int a[10][10], transpose [10][10],r,c,i,j;
	printf("Enter the number of rows and columns:");
	scanf("%d%d",&r,&c);
	printf("Enter the matrix elements:\n");
	for (i=0;i<r;i++)
	{
		for (j=0;j<c;j++)
		{
			printf("Enter the element %d%d:",i,j);
			scanf("%d",&a[i][j]);
		}
	}
	printf("The entered matrix is:\n");
	for (i=0;i<r;i++)
	{
		for(j=0;j<c;j++)
		{
			printf("%d",a[i][j]);
		}
	}
	for (i=0;i<r;i++)
	{
		for (j=0;j<c;j++)
		{
			transpose[j][i]=a[i][j];
		}
	}
	printf("Transpose of the matrix:\n");
	for (i=0;i<c;i++)
	{
		for(j=0;j<r;j++)
		{
			printf("%d",transpose[i][j]);
			if(j==r-1)
			{
				printf("\n");
			}
		}
	}
	return 0;
}

