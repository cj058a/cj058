#include<stdio.h>
#include <math.h>
int main()
{
	int a, b, c;
	float r1, r2, r3,x, img;
	printf("Enter the values of a, b and c of the quadratic equation");
	scanf("%d%d%d",&a,&b,&c);
	x=(b*b)-(4*a*c);
	if(x>0)
	{
		r1=(-b)-sqrt(x)/(2*a); 
		r2=(-b)+sqrt(x)/(2*a);
		printf("The roots are real- %f and %f\n",r1, r2);
	}
	else if (x==0)
	{
		r3=(-b)/(2*a);
		printf("The root is real and distinct-%f",r3);
	}
	else
	{   
		img=sqrt(x);
		r3=(-b)/(2*a);
		printf("the roots are imaginary-%f+%fi and %f-%fi\n",r3,img,r3,img);
	}
	return 0;
}
