#include<stdio.h>
int main ()
{
	int x, y, *a, *b, temp;
	printf("Enter the values of x and y\n");
	scanf("%d%d",&x,&y);
	printf("The numbers before swapping are: x=%d\n y=%d\n",x,y);
	a=&x;
	b=&y;
	temp=*b;
	*b=*a;
	*a=temp;
	printf("The numbers after swapping are: x=%d\n y=%d\n",x,y);
	return 0;
}